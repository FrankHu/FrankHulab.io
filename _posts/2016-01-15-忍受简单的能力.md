---
layout: article
title: 忍受简单的能力
date:   2016-01-15 22:17
description: 仰望星空，脚踏实地
category: life
image:
    #feature: /teaser/besimple.jpg
    teaser: /teaser/besimple.jpg
---

>  摘要： {{page.description}}

---
![opinion](/images/teaser/besimple.jpg)




> 其实想要过好一生并没有什么特别复杂的道理，都是些简单的，但却难以坚持的东西。比如：坚持锻炼、坚持阅读、坚持自我更新每天进步一点点。

### 忍受简单的能力
    对于聪明人来说，最难以忍受的情况不是一件事有多难，而是纯粹的简单。
    没有难度挑战的任务，会让他们感到无所着力，继而注意力涣散，重复的练习是他们的死穴。


我们向往不平凡的人生，向往随心所欲、无所不能、酣畅淋漓的人生的体验，这固然不是错，这确实是人类的本性，永远对自己的生活抱有更高的期望，也是人类不断进步的原动力。但是，平平淡淡很多时候确实才是生活的本质，而且很多时候恐怕你也不得不承认其实你并不比你身边的人太强到哪里去，你也不是小时候那个自以为自己长大以后无所不能的超人，所以往往面对生活的不完美，你也只能选择默默忍受着。假如你问一下自己一年365天，是平平淡淡的日子多还是惊心动魄的日子多，那么我想你的答案一定是前者，至于平平淡淡的日子到底比惊心动魄的日子多多少，这恐怕你比我更心知肚明。很多时候，恰恰是这些看似平平淡淡的日子充实着我们的人生，所以要如何面对这大量的平平淡淡的一天，是我们每一个平凡的人都要认真考虑的问题。

你是抱怨还是默默忍受并积蓄力量？你是乐观还是悲观？你是每天坚持进步每天改善一点点还是得过且过逐渐迷失自我？人生就是不完美的，这是一个伟大的真理。面对问题、面对平凡，你会感觉到痛苦、苦闷；然而认识到这一点并接受事实才是我们心智成熟的第一步。第一步走好了，往后就好走了，至少走起来痛苦也会比原来减轻一些。

认清事实并坦然接受是第一步，第二步更重要的则是学会在平淡的日子里保持耐心，踏踏实实地走好自己该走的路。而这往往很难，说难并不是说这事需要很大的智力、体力、财力或物力，而是说我们需要能够忍受目前平淡的生活，默默忍受着目前生活的不完美之处，并踏踏实实做好自己该做的事，积蓄养分，不断成长，强化自己的力量。

在这个过程中，我曾看到很多聪明人，他们确实真切地感受到了生活的平淡与不完美，也很快知道自己需要做什么来改变现状，但却往往非常可惜的是，他们缺少耐心。是的，他们的大脑往往运转速度很快，但因为脑子太快了，所以需要身体下工夫的地方，往往就有种种困难。锻炼身体时，他们往往会想：[这样跑步真的有用吗？]、[有没有什么速成的锻炼方法，能够不这样枯燥乏味？]，想法多、脑子转的快本身没有错，错的是思维上的变化多端，往往就造成了行动层面的进步迟缓。

**通过日常的生活观察我发现真正最影响一个人的成就的因素，可能不是智商，也不是努力，而在于他有多踏实。**踏实的人做一件事，是一件事；学一样东西，就学得到一样东西。你只要看一门课最开始的时候，讲一些最简单的知识，哪些人可以不厌其烦地听进去，他们未来就算没有什么惊人的成就，也都不会混得太差。而聪明人往往已失去了耐心，都趴在桌子上睡觉。还记得我高中的一同桌，现在想想其实也不是一个聪明、特别厉害的人，每次数学最后一个大题他基本都做不出来，而我则隔三差五地还能碰对。然而他的成绩却每次都比我好，总是在年级前两百，从未掉过队。后来也是顺利地考上了牛叉的华中科技大学，比我强多了。我后来分析，他的成功很大程度上来源于他的踏实程度，他会把每一道错题认真记载，时常复习，每天做一套模拟卷子，雷打不动，这些都是当时年轻的我不曾有过的踏实。

所以，对一个学习者来说，这是最好的时代，也是最坏的时代。今天的信息不是匮乏而是整个地泛滥了，你知道自己生活的不完美，但你很难让自己真的不去焦虑。为了克服这种焦虑，摆脱心中的弱小感，我们经常逼迫自己学习这样或那样的东西。我们看到要学的东西越之多，便感觉自己越渺小。越渺小便越不能忍受目前自己正在学习的平淡的，相比渺小的东西，便越是焦虑不安，越是无法脚踏实地，一点点地不断进步，不断突破。
所以，如果真的想学一点东西，就需要一种特别的能力。我把它叫做「**忍受简单的能力**」。我不知道是叫能力还是勇气更好，因为它涉及到了一种真正意义上的放弃。——当你在某一个点上停下来，打算认真下点工夫的时候，这意味着放弃想象中的其它可能。你得到的可能只是简单的一点点，面对却是巨大的不确定性，这或许也是一种勇气吧。
