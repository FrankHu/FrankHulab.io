---
layout: article
title: About me
date:   2015-05-26 15:18
permalink: /about/
share: false
image: 
    feature: about.jpg
    teaser: about
---

关于我

* 胡孝林
* 长江大学 软件工程 本科
* Java工程师，C#、Python爱好者
* 经历：
    - Java软件开发工程师 搜房网 一年
- 联系方式：739272238@qq.com
